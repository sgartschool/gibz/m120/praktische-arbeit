# Praktische Arbeit - Modulprüfung 120

C# WPF Applikation mit Anwendung von MVVM um Währungen umzurechnen

---

## Konzeptioneller Teil

- Steuerbarkeit
- Fehlertoleranz

**Gelungene Aspekte**

- SB: Umrechnung erfolgt auf Eingabe, dadurch ist kein Knopfdruck des Benutzers nötig.

**Verbesserungsmöglichkeiten**

- FT: Applikation stürzt bei Eingabe eines Strings ab, da nur auf leer/nicht leer geachtet wird.