﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using Model;


namespace ViewModel
{
    public class Controller : ModelBase
    {
        ConversionData conversionData = new ConversionData();

        private string _sourceType;
        private string _targetType;
        public string sourceType { get => _sourceType; set { _sourceType = value; } }
        public string targetType { get => _targetType; set { _sourceType = value; } }

        // Dummy Currencies for Testing
        Currency sourceCurrency = new Currency("CHF", "Schweizer Franken");
        Currency targetCurrency = new Currency("EUR", "Euro");

        private double _value;
        public string source { get => Convert.ToString(_value); set { _value = GetRoundedValue(value); sourceChanged("source"); } }
        public string target { get => Convert.ToString(GetRoundedValue(_value, targetCurrency, sourceCurrency)); set { _value = GetRoundedValue(value, sourceCurrency, targetCurrency); sourceChanged("target"); } }
        private void sourceChanged(string source)
        {
            OnPropertyChanged(source);
        }

        private double GetRoundedValue(string value)
        {
            if (value == null || value.Trim() != "")
                return 0.00;
            return Math.Round(Convert.ToDouble(value), 2);
        }
        private double GetRoundedValue(double value)
        {
            return Math.Round(value, 2);
        }
        private double GetRoundedValue(string value, Currency sourceCurrency, Currency targetCurrency)
        {
            if (value == null || value.Trim() == "")
                return 0.00;
            return Math.Round(ConversionData.Convert(Convert.ToDouble(value), sourceCurrency, targetCurrency), 2);
        }
        private double GetRoundedValue(double value, Currency sourceCurrency, Currency targetCurrency)
        {
            return Math.Round(ConversionData.Convert(Convert.ToDouble(value), sourceCurrency, targetCurrency), 2);
        }
    }

    public class ModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
